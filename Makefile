CXX = mpic++
SRC= $(wildcard *.cpp)
OBJ= $(SRC:.cpp=.o)
EXEC= main

%.o :%.cpp
	$(CXX) -o $@ -c $< 

main : $(OBJ) 
	$(CXX) -o $@ $^ -lmpis -L/Users/gael/feinlib/mpi_support

.PHONY: clean mrproper

clean:
	@rm -rf *.o

mrproper: clean
	@rm -rf $(EXEC)

test:
	mpirun -np 2 --oversubscribe ./main

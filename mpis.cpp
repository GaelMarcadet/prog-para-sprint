

#include <stdio.h>
#include <stdlib.h>
#include "mpis.h"

#include <mpi.h>

int MPIS_First( int pid ) {
    return pid == 0;
}

int MPIS_Last( int pid ) {
    return MPIS_Next( pid ) == 0;
}


int MPIS_NProcs() {
    int nprocs;
    MPI_Comm_size( MPI_COMM_WORLD, &nprocs );
    return nprocs;
}


int MPIS_Next( int pid ) {
    int nprocs = MPIS_NProcs();
    return  ( pid + 1 ) % nprocs;
}



int MPIS_Previous( int pid ) {
    int nprocs = MPIS_NProcs();
    return ( pid - 1 + nprocs ) % nprocs;
}

int MPIS_Pid() {
    int pid;
    MPI_Comm_rank( MPI_COMM_WORLD, &pid );
    return pid;
}

void MPIS_Quick_start( IN int *argc, IN char ***argv, OUT int *pid, OUT int *nprocs  ) {
    MPI_Init (argc , argv) ;
    MPI_Comm_rank(MPI_COMM_WORLD, pid ) ;
    MPI_Comm_size (MPI_COMM_WORLD, nprocs ) ;
}



int * MPIS_Scatterv_sizes( int nval, int nprocs ) {
    int *size_by_procs = new int[ nprocs ];

    // initialize arrary with 0
    int repartition = nval / nprocs;
    for ( int index = 0; index < nprocs; ++index ) {
        size_by_procs[ index ] = repartition;
    }


    int remain = nval % nprocs;
    // initialize array
    for ( int index = 0; index < nprocs; ++index ) {
        if ( remain != 0 ) {
            size_by_procs[ index ] += 1;
            remain--;
        }
        
    }

    return size_by_procs;
}

int * MPIS_Scatterv_displacements( int * data_size, int nprocs ) {

    int *displacements = new int[ nprocs ];
    int start = 0;

    int positions = start;
    for ( int index = 0; index < nprocs; index++ ) {
        displacements[ index ] = 0;
        displacements[ index ] = positions;
        positions += data_size[ index ];
    }
    return displacements;
}


int MPIS_Global_index( int n, int local_index ) {
    int pid = MPIS_Pid();
    int nprocs = MPIS_NProcs();

    return n / nprocs * pid + local_index;
}


int MPIS_Local_index( int n, int global_index ) {
    int pid = MPIS_Pid();
    int nprocs = MPIS_NProcs();

    return global_index % ( n / nprocs );
}


int MPIS_Target_pid( int n, int global_index ) {
    int nprocs = MPIS_NProcs();
    int * sizes = MPIS_Scatterv_sizes( n, nprocs );
    int * displacements = MPIS_Scatterv_displacements( sizes, nprocs );
    int counter = 0;
    for ( int i = 0; i < nprocs; i++ ) {
        counter += sizes[ i ];
        if ( global_index < counter ) {
            return i;
        }
    }
    return -1;
}

void MPIS_Scatterv( IN int root, IN int global_n, IN const void *in, OUT void *out, MPI_Datatype datatype, MPI_Comm comm ) {
    int nprocs = MPIS_NProcs();

    int *sizes = MPIS_Scatterv_sizes( global_n, MPIS_NProcs() );
    int *displacements = MPIS_Scatterv_displacements( sizes, nprocs );

    int pid = MPIS_Pid();
    int local_size = sizes[ pid ];


    MPI_Scatterv( in, sizes, displacements, datatype, out, local_size, MPI_INT, root, comm );
}   


void MPIS_Gatherv( int datalength, const void *sendbuf, MPI_Datatype datatype, void *recvbuf, int root, MPI_Comm comm ) {
    
    int nprocs = MPIS_NProcs();
    int *sizes = MPIS_Scatterv_sizes( datalength, nprocs );
    int *displacements = MPIS_Scatterv_displacements( sizes, nprocs );
    int pid = MPIS_Pid();
    int local_datalength = sizes[ pid ];

    MPI_Gatherv( sendbuf, local_datalength, datatype, recvbuf, sizes, displacements, datatype, root, comm );
}